class Clock
end

class AlarmClock < Clock  
  def initialize(given_time, message)
    @time_formatting = /\d\d:\d\d:\d\d/
    @start_time = Time.now.to_s.scan(@time_formatting)[0]
    @given_time = given_time
    @message = message
  end
  
  def check_time
    puts @message if Time.now.to_s.scan(@time_formatting)[0] == @given_time
  end
  
  def clock_function
    until Time.now.to_s.scan(/\d\d:\d\d:\d\d/) == @given_time
      sleep 0.5
      check_time
    end
    return
  end
end

z = AlarmClock.new('00:48:44', "The time you selected has been reached")
z.clock_function